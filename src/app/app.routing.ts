import { UserModule } from './user/user.module';
import { HomeComponent } from './home/home.component';
import { Route, Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from './not-found/not-found.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    {
        path: 'users',
        loadChildren: () => UserModule
    }
    // { path: '**', component: NotFoundComponent }, //always last
];

export const AppRouting = RouterModule.forRoot(appRoutes, {
    useHash: true
});
// {path:'listes' ,loadChildren: ()=>ListModule}
//  not 
//  {path:'listes' ,loadChildren: 'app/component/list/list.module#ListModule'}