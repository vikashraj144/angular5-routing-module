import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { UsersRouting, usersRoutes } from './users.routing';
import { RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';

export const routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'users', component: UserComponent, data: { breadcrumb: 'third' } },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(usersRoutes)
  ],
  declarations: [UserComponent, ProfileComponent]
})
export class UserModule { }
