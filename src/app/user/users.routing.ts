import { UserComponent } from './user/user.component';
import { Routes, RouterModule } from "@angular/router";
import { ProfileComponent } from './profile/profile.component';


export const usersRoutes: Routes = [
    { path: '', component: UserComponent }
    { path: 'profile', component: ProfileComponent, pathMatch: 'full' }
];

export const UsersRouting = RouterModule.forChild(usersRoutes);
